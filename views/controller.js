
'use strict';

var app = angular.module('myApp', []);
app.controller('myCtrl', function ($scope, $http) {

  $scope.skills = ["JavaScript", "JQuery", "AngularJS", "NodeJS", "ExpressJS", "MongoDB"];
  
  $scope.insertFun = function (stu) {
    var uname = $scope.stu.Username;
    var uid = $scope.stu.Userid;
    var sskills = $scope.stu.selectedSkills;
    //alert("the result is"+uname);
    //alert("the result is"+uid);
    $http.post('/addname', {
      "username": uname,
      "userid": uid,
      "selectedskills": sskills
    }).then(function (response) {
      //$http.get('/addname/'+uname+'/'+uid)
      alert("Response from Server****************" + response.data);
      $scope.myWelcome = response.data;
    });
  };

  $scope.updateFun = function (stu) {
    var uname = $scope.stu.Username;
    var uid = $scope.stu.Userid;
    var sskills = $scope.stu.selectedSkills;
    //alert("the result is"+uname);
    //alert("the result is"+uid);
    $http.post('/update', {
      "username": uname,
      "userid": uid,
      "selectedskills": sskills
    }).then(function (response) {
      //$http.get('/addname/'+uname+'/'+uid)
      alert("Response from Server****************" + response.data);
      $scope.myWelcome = response.err;
    });
  };

  $scope.deleteFun = function (stu) {
    var uname = $scope.stu.Username;
    var uid = $scope.stu.Userid;
    alert("the result is" + uname);
    alert("the result is" + uid);
    $http.delete('/delete/' + uid).then(function (response) {
      //$http.get('/addname/'+uname+'/'+uid)
      alert("Response from Server****************" + response.data);
      $scope.myWelcome = response.data;
    });
  };

});

app.controller('myCtrl1', function ($scope, $http) {

  $http.get('/finddata').then(function (response) {
    //$http.get('/addname/'+uname+'/'+uid)
    console.log("Response from Server****************" + JSON.stringify(response));
    //$state.go('table.html', 
    $scope.myWelcome = response.data;
    //);

  });
});
