var express = require("express");
var bodyParser = require('body-parser');
var app = express();
var port = 3000;
var mongoose = require("mongoose");

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/student");

var nameSchema = new mongoose.Schema({

  fname: String,
  fid: String,
  fskills: Array
});


var User = mongoose.model("details", nameSchema);


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.post("/addname", (req, res) => {
  console.log("request param" + req.body.username);

  //res.send("username from server"+ req.body);
  //console.log("req.body"+req.body);
  var myData = new User();
  myData.fname = req.body.username;
  myData.fid = req.body.userid;
  myData.fskills = req.body.selectedskills;

  myData.save()
    .then(item => {
      res.send("item saved to database");
    })
    .catch(err => {
      res.status(400).send("unable to save to database");
    });
});

app.post("/update", (req, res) => {
  //var myData = new User();
  console.log("fname" + req.body.username);
  console.log("fid=" + req.body.userid);
  User.update({
    "fid": req.body.userid
  }, {
    "$set": {
      "fname": req.body.username,
      "fskills":req.body.selectedskills
    }
  }).then(function (err, data) {
    // myData.save()
    // console.log("",myData);
    if (err) {
      console.log();
      res.send("error" + JSON.stringify(err));
    }
    if (data) {
      console.log("***************", data);
      res.send("item updated to database" + JSON.stringify(data));
    }
  })
});

app.delete("/delete/:uid", (req, res) => {
  var id = req.params.uid;
  User.findOneAndRemove({
    "fid": id
  }).then(function (err, data) {
    if (err) {
      console.log();
      res.send("error" + JSON.stringify(err));
    }
    if (data) {
      console.log("***************", data);
      res.send("item deleted to database" + JSON.stringify(data));
    }
  })
});

app.get("/finddata", (req, res) => {
      User.find({}).then(function (err, data) {
        if (err) {
          console.log("Error from server", JSON.stringify(err));
          res.send(err);
        } else {
          console.log("data from db********", data);
          res.send(data);
        }

      });

});

app.use("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

app.listen(port, () => {
  console.log("Server listening on port " + port);

});

module.exports=app;